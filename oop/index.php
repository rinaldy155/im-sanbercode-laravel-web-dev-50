<?php
require_once 'animal.php';
require_once 'Frog.php';
require_once 'Ape.php';

$sheep = new Animal("Shaun");
$sheep->displayInfo();
echo "<br>";

$kodok = new Frog("Buduk");
$kodok->displayInfo();
$kodok->jump();
echo "<br>";
echo "<br>";

$sungokong = new Ape("Kera Sakti");
$sungokong->displayInfo();
$sungokong->yell();

?>

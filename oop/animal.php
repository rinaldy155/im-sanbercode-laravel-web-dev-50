<?php
class Animal {
    public $name;
    public $legs = 4;
    public $cold_blooded = "no";

    public function __construct($name) {
        $this->name = $name;
    }

    public function displayInfo(){
        echo "Name: " . $this->name . "<br>";
        echo "Legs: " . $this->legs . "<br>";
        echo "Cold blooded: " . $this->cold_blooded . "<br>";
        
    }
}
?>

<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TableController;
use App\Http\Controllers\DataTableController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);
Route::get('/table', [TableController::class, 'tabel'])->name('tabel'); 
Route::get('/data-table', [DataTableController::class, 'datatable'])->name('data-table');
// testing
Route::get('/master', function(){
    return view ('layouts.master');
});

// CRUD

// Create Data
//Mengarah ke form tambah cast
Route::get('/cast/create', [CastController::class, 'create']);
// untuk mengirim data inputan ke table cast di db
Route::post('/cast', [CastController::class, 'store']);

//Read Data
//Mengarah ke halaman tampil semua data cast
Route::get('/cast', [CastController::class, 'index']);
// Mengarah ke tampil data berdasarkan parameter id
Route::get('/cast/{id}', [CastController::class, 'show']);

// Update Data
// Mengarah ke form edit data berdasarkan parameter id
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
//Untuk UPDATE Data berdasarkan parameter id
Route::put('/cast/{id}', [CastController::class, 'update']);

//Delete Data
// Hapus Data berdasarkan parameter id
Route::delete('/cast/{id}', [CastController::class, 'destroy']);

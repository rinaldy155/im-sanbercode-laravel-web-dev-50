@extends('layouts.master')
@section('title')
Tampil Cast
@endsection
@section('content')

<a href="/cast/create" class="btn btn-primary my-3">Tambah Cast</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key => $item)
      <tr>
        <th scope="row">{{$key + 1}}</th>
        <td>{{$item->nama}}</td>
        <td>
            <form action="/cast/{{$item->id}}" method="POST">
                @csrf
                @method('DELETE')
                <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-secondary btn-sm">Edit</a>
                <input type="submit" value="delete" class="btn btn-danger btn-sm">
            </form>
        </td>
      </tr>    
      @empty    
      <tr>
        <td>Tidak Ada Cast</td>
      </tr>
      @endforelse
      
    </tbody>
  </table>

@endsection
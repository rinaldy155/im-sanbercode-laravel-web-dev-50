@extends('layouts.master')
@section('title')
Tambah Cast
@endsection
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form method="POST" action="/cast">
    @csrf
    <div class="form-group">
      <label>Cast Nama</label>
      <input type="text" class="form-control" name="nama">
    </div>
    <div class="form-group">
      <label>Umur</label>
      <input type="text" class="form-control" name="umur">
    </div>
    <div class="form-group">
      <label>Bio</label>
      <textarea name="bio" cols="30" rows="10" class="form-control"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
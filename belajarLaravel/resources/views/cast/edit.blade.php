@extends('layouts.master')
@section('title')
Edit Cast
@endsection
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form method="POST" action="/cast/{{$cast->id}}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Cast Nama</label>
      <input type="text" class="form-control" name="nama" value="{{$cast->nama}}">
    </div>
    <div class="form-group">
      <label>Umur</label>
      <input type="text" class="form-control" name="umur "value="{{$cast->umur}}">
    </div>
    <div class="form-group">
      <label>Bio</label>
      <textarea name="bio" cols="30" rows="10" class="form-control">{{$cast->bio}}</textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection